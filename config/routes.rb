Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get '/tags' => 'tags#index'
  get '/tags/:id' => 'tags#show', as: :tag

  get '/destinations/:id' => 'destinations#show', as: :destination
  get '/destinations/:id/edit' => 'destinations#edit', as: :edit_destination
  patch '/destinations/:id' => 'destinations#update'
  # Defines the root path route ("/")
  # root "articles#index"
end
